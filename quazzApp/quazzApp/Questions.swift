//
//  Questions.swift
//  quazzApp
//
//  Created by Milica Ignjatovic on 9/10/17.
//  Copyright © 2017 Milica Ignjatović. All rights reserved.
//

import UIKit

struct StructQuestion{
    var type = 0.0
    var que = String()
    var offAnswer: [String] = Array()
    var correctAnsver = String ()
}

class Questions: NSObject {

    func parsingQue (getQuestions:NSDictionary)->[StructQuestion]{
        var questions : [StructQuestion] = Array()
        var structQuestion : StructQuestion = StructQuestion()
        let dict = getQuestions.allValues
        let arrayQuestions = dict[0] as! NSDictionary
        for element in arrayQuestions {
            let unparsedValueArray = element.value as! NSArray
            if (unparsedValueArray.count > 0){
                let unparsedValue = unparsedValueArray[0] as! NSArray
                structQuestion.type = (unparsedValue[4] as! NSString).doubleValue
                structQuestion.que = unparsedValue [1] as! String
                structQuestion.correctAnsver = unparsedValue [3] as! String
                let str = unparsedValue [2] as! String
                structQuestion.offAnswer = str.components(separatedBy: ",")
                if structQuestion.type == 1{
                    if structQuestion.offAnswer.count == 3{
                        questions.append(structQuestion)
                    }
                }else {
                    questions.append(structQuestion)
                }
            }
        }
        return questions
    }

}
