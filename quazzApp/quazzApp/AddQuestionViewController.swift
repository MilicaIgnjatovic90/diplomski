//
//  AdminViewController.swift
//  quazzApp
//
//  Created by Milica Ignjatovic on 9/3/16.
//  Copyright © 2016 Milica Ignjatović. All rights reserved.
//

import UIKit

class AddQuestionViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
    @IBOutlet weak var question: UITextField!
    @IBOutlet weak var correncAns: UITextField!
    @IBOutlet weak var offeredAns: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    var list = ["open question", "check box question", "date queston"]
    var type = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.reloadAllComponents()
    }
    
    @IBAction func sendQuestion(_ sender: UIButton) {
        OperationQueue.main.addOperation {

            if self.verifyQuestonData(){
                self.sendQuestionToServer()
            }
        }
    }

    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return list.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.view.endEditing(true)
        return list[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.type = row
    }
    
    func sendQuestionToServer(){
        ProxyInterface.sharedInstance.postQuestion(question.text! as NSString, answers:offeredAns.text!, corectAnswer: correncAns.text! as NSString, typeOfQuestion: type, completion: {(retVal) -> Void in
                if (retVal == "True"){
                    self.questoinSent()
                    self.question.text = ""
                    self.offeredAns.text = ""
                    self.correncAns.text = ""
                }
        })
    }
    
    func verifyQuestonData()->Bool{
        if (self.question.text?.isEmpty)!{
            questionText()
            return false
        }else {
            if (self.correncAns.text?.isEmpty)!{
                corectAnswer()
                return false
            }
        }
        
        if self.type == 0 || self.type == 2{
            if !(self.offeredAns.text?.isEmpty)!{
                noOfferedAnswers()
                return false
            }
        }else {
            if self.type == 1{
                if self.offeredAns.text != " "{
                    if self.offeredAns.text!.components(separatedBy: ",").count == 3{
                        return true
                    }else {
                        offeredAnswers()
                        return false
                    }
                }else {
                    return true
                }
            }
        }
        return false
    }
    
    fileprivate func questionText() {
        let alertController = UIAlertController(title: "Question", message:
            "Please enter queston text", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    fileprivate func somethingWentWrong() {
        let alertController = UIAlertController(title: "Question", message:
            "Something went wrong, please try again", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    fileprivate func corectAnswer() {
        let alertController = UIAlertController(title: "Question", message:
            "Must have correct answer", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    fileprivate func noOfferedAnswers() {
        let alertController = UIAlertController(title: "Question", message:
            "Must have no offered answers", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func offeredAnswers() {
        let alertController = UIAlertController(title: "Question", message:
            "Must have three offered answers", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func questoinSent() {
        let alertController = UIAlertController(title: "Question sent", message:
            "", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
