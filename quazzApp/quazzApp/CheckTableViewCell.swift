//
//  CheckTableViewCell.swift
//  quazzApp
//
//  Created by Milica Ignjatovic on 9/4/16.
//  Copyright © 2016 Milica Ignjatović. All rights reserved.
//

import UIKit

class CheckTableViewCell: UITableViewCell {

    @IBOutlet weak var cell: UIView!
    
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var thirdChoice: CheckBox!
    @IBOutlet weak var firstChoice: CheckBox!
    @IBOutlet weak var secondChoice: CheckBox!
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var correctAnswer: UILabel!

    var correctAns : String = " "
    var correct: Bool = false

    @IBAction func secondBtn(_ sender: CheckBox) {
        if secondLabel.text!.contains(correctAns){
            Score.finalScore = Score.finalScore + 1
            backgroundColor = UIColor.green
            correctAnswer.isHidden = true
        }else {
            setCellForUncorretAnswer()
        }
    }
    
    @IBAction func firstBtn(_ sender: CheckBox) {
        if firstLabel.text!.contains(correctAns) {
            Score.finalScore = Score.finalScore + 1
        }else {
            setCellForUncorretAnswer()
        }
    }
    
    @IBAction func thirdBtn(_ sender: CheckBox) {
        if thirdLabel.text!.contains(correctAns) {
            Score.finalScore = Score.finalScore + 1
            correct = true
        }else {
            setCellForUncorretAnswer()
        }
    }
    
    func setCellForUncorretAnswer(){
        correctAnswer.text = correctAns
        firstChoice.isEnabled = false
        secondChoice.isEnabled = false
        thirdChoice.isEnabled = false
        backgroundColor = UIColor.red
        correctAnswer.isHidden = false
    }
    
    override func awakeFromNib() {
        correctAnswer.isHidden = true
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

           }

}
