//
//  UserDefaultsHelper.swift
//  quazzApp
//
//  Created by Milica Ignjatovic on 9/10/17.
//  Copyright © 2017 Milica Ignjatović. All rights reserved.
//

import UIKit

class UserDefaultsHelper: NSObject {
    
    func readUserDefaults()->String{
        let defaults = UserDefaults.standard
        var string:String = defaults.value(forKey: "numQuestions") as! String
        if  string.isEmpty{
            string = "10"
        }
        return string
    }
    
    func writeUserDefaults(string: String){
        let userDefaults = UserDefaults.standard
        userDefaults.setValue(string, forKey: "numQuestions")
    }
}
