//
//  ScoreViewController.swift
//  quazzApp
//
//  Created by Milica Ignjatovic on 9/18/16.
//  Copyright © 2016 Milica Ignjatović. All rights reserved.
//

import UIKit

class ScoreViewController: UIViewController {

    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var scoreLbl: UILabel!
    var value:String = ""
    
    
    
    @IBAction func scoreData(_ sender: UIButton){
        if !(username.text?.isEmpty)! && !(password.text?.isEmpty)! {
            ProxyInterface.sharedInstance.scoreFunc(username.text!, password: password.text!, completion: {(retVal) -> Void in
                self.value = retVal
                if retVal == "True" {
                    self.scoreLbl.text = retVal
                }else{
                    self.wrongData()
                }
            })
        }
    }
    
    fileprivate func wrongData() {
        let alertController = UIAlertController(title: "Wrong data ", message:
            self.value, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
    }

}
