//
//  Proxy.swift
//  quazzApp
//
//  Created by Milica Ignjatovic on 8/30/16.
//  Copyright © 2016 Milica Ignjatović. All rights reserved.
//

import UIKit
let LOCAL_IP = "127.0.0.1"
enum typeOfAction {
    case login
    case register
    case postQue
    case getQue
    case score
}
typealias RequestLoaderCompletion = (_ data: Data?, _ error: Error?) -> Void
typealias RequestLoaderJsonCompletion = (_ json: AnyObject?, _ error: Error?) -> Void


class Proxy: NSObject, URLSessionDataDelegate {
    var questions : [StructQuestion] = Array()

    func  authorize() {

    }
    
    var isAcepted = false;
    var ret:NSDictionary = ["": NSDictionary()]
    
    func postToServerData(_ localIpGetScenario:String,params:NSDictionary, method: String, typeOfAct:typeOfAction, completion: @escaping (NSDictionary) -> Void){

        let url = URL(string:localIpGetScenario);
        let request = NSMutableURLRequest (url:url!);
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = method

        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        }
        catch {
            
        }
        
        _ = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if let httpResponse = response as? HTTPURLResponse {
                if (httpResponse.statusCode > 199 && httpResponse.statusCode < 300 ){
                    self.ret = self.processDataFromServer(data!, typeOfAct:typeOfAct)
                    completion(self.ret)
                }
                else{
                    print ("status code is:", httpResponse.statusCode)
                }
            }
        }
        
        task.resume()
    }
    
    func processDataFromServer(_ data:Data, typeOfAct:typeOfAction) -> NSDictionary{
        let type = typeOfAct
        switch type {
        case .login, .register, .score:
            
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                let responseFromServer: String = (json as AnyObject).object(forKey: "val") as! String
                let dict : NSDictionary = [responseFromServer : [:]]
                return dict
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            
        case .postQue:
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                let responseFromServer: String = (json as AnyObject).object(forKey: "val") as! String
                let dict : NSDictionary = [responseFromServer : [:]]
                return dict
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
        case .getQue:
            do {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    let dict : NSDictionary = json as! NSDictionary
                    return dict
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        return ["": [:]]
    }
    
    func retQuestionFunc(_ data:Data) -> (Bool, NSArray) {
        do{
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            return (false, json as! NSArray)
        }
        catch{
          
        }
        return (false, [])
    }
    
    func loginFunc(_ username: String, password: String, completion: @escaping (String) -> Void) {
        let localIpGetScenario = String (format:"http://%@:8888/login/",LOCAL_IP);
        let params = ["username": username, "password": password]
        let method = "POST"
        postToServerData(localIpGetScenario, params: params as NSDictionary, method: method, typeOfAct: .login, completion: { (retValue) -> Void in
            completion(retValue.allKeys[0] as! String)
        })
    }
    
    func registerFunc(_ username: String, password: String, email : String, completion: @escaping (String) -> Void) {
        let localIpGetScenario = String (format:"http://%@:8888/registre/",LOCAL_IP);
        let params = ["username": username, "password": password, "email": email]
        let method = "POST"
        postToServerData(localIpGetScenario, params: params as NSDictionary, method: method, typeOfAct: .register, completion: { (retValue) -> Void in
            completion(retValue.allKeys[0] as! String)
        })
    }
    
    func postQuestion(_ queston: NSString, answers: String, corectAnswer:NSString, typeOfQuestion:NSInteger, completion: @escaping (String) -> Void) {
        let localIpGetScenario = String (format:"http://%@:8888/questions/",LOCAL_IP);
        let params = ["queston":queston, "answers": answers, "corectAnswer":corectAnswer, "typeOfQuestion": typeOfQuestion] as [String : Any]
        let method = "POST"
        postToServerData(localIpGetScenario, params: params as NSDictionary, method: method, typeOfAct: .postQue, completion: { (retValue) -> Void in
            completion(retValue.allKeys[0] as! String)
        })
    }
    
    func scoreFunc(_ username: String, password: String, completion: @escaping (String) -> Void) {
        let localIpGetScenario = String (format:"http://%@:8888/score/",LOCAL_IP);
        let params = ["username": username, "password": password]
        let method = "GET"
        postToServerData(localIpGetScenario, params: params as NSDictionary, method: method, typeOfAct: .score, completion: { (retValue) -> Void in
            completion(retValue.allKeys[0] as! String)
        })
    }

    func postScoreFunc(_ username: String, score: String, completion: @escaping (String) -> Void) {
        let localIpGetScenario = String (format:"http://%@:8888/score/",LOCAL_IP);
        let params = ["username": username, "score": score]
        let method = "POST"
        postToServerData(localIpGetScenario, params: params as NSDictionary, method: method, typeOfAct: .score, completion: { (retValue) -> Void in
            completion(retValue.allKeys[0] as! String)
        })
    }
    
    func getQuestions(completion: @escaping (NSDictionary) -> Void){
        let localIpGetScenario = String (format:"http://127.0.0.1:8888/getQuestions/");
        let params = ["username": "", "questionNum":UserDefaultsHelper().readUserDefaults()]
        let method = "POST"
        postToServerData(localIpGetScenario, params: params as NSDictionary, method: method, typeOfAct: .getQue, completion: { (retValue) -> Void in
            completion(retValue)
        })
    }
}
