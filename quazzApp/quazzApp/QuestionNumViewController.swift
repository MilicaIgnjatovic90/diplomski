//
//  QuestionNumViewController.swift
//  quazzApp
//
//  Created by Milica Ignjatovic on 8/21/17.
//  Copyright © 2017 Milica Ignjatović. All rights reserved.
//

import UIKit

class QuestionNumViewController: UIViewController {
    @IBOutlet weak var numQuestion: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func saveNumOfQuestions(_ sender: UIButton) {
        if numQuestion.hasText{
            UserDefaultsHelper().writeUserDefaults(string: numQuestion.text!)
            savedTextFiled()
        }else{
            emptyTextFiled()
        }
    }
    
    fileprivate func emptyTextFiled() {
        let alertController = UIAlertController(title: "", message:"Please enter number of questions", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    fileprivate func savedTextFiled() {
        let alertController = UIAlertController(title: "", message:"Saved number of questions", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
}
