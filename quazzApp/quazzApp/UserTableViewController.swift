//
//  UserTableViewController.swift
//  quazzApp
//
//  Created by Milica Ignjatovic on 9/3/16.
//  Copyright © 2016 Milica Ignjatović. All rights reserved.
//

import UIKit

class UserTableViewController:UIViewController, UITableViewDelegate, UITableViewDataSource, URLSessionDataDelegate {
    var username: String = ""
    var questions : [StructQuestion] = Array()
    
    @IBOutlet var tableView: UITableView!
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Score.finalScore = 0
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getQuestions()
    }
    
    //MARK:get questions from server
    func getQuestions(){
        ProxyInterface.sharedInstance.getQuestions(completion: {(retVal) -> Void in
            self.questions = Questions().parsingQue(getQuestions: retVal)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })

    }
    
    //MARK: Table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.questions.count
    }
    
    func tableView (_ tableView:UITableView, heightForRowAt indexPath:IndexPath) -> CGFloat{
        return 250.0;
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell {
        return instantiateCellForIndexPath(indexPath: indexPath)
    }
    
    
    func instantiateCellForIndexPath(indexPath:IndexPath)-> UITableViewCell{
        if (self.questions.count > 0){
            switch self.questions[indexPath.row].type {
            case _ where self.questions[indexPath.row].type == 0:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "enterTableViewCell", for: indexPath as IndexPath) as! EnterTableViewCell
                cell.title.text = self.questions[indexPath.row].que
                cell.correctAns = self.questions[indexPath.row].correctAnsver
                return cell
            case _ where self.questions[indexPath.row].type == 1:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "checkTextCell", for: indexPath as IndexPath) as! CheckTableViewCell
                cell.question.text = questions[indexPath.row].que
                let oof = questions[indexPath.row]
                cell.firstLabel.text = oof.offAnswer[0]
                cell.secondLabel.text = oof.offAnswer[1]
                cell.thirdLabel.text = oof.offAnswer[2]
                cell.correctAns = questions[indexPath.row].correctAnsver
                return cell
            case _ where self.questions[indexPath.row].type == 2:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "dateTextCell", for: indexPath as IndexPath) as! dateCellTableViewCell
                cell.question.text = self.questions[indexPath.row].que
                cell.correctAns = self.questions[indexPath.row].correctAnsver
                return cell
            default:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "emptyCell", for: indexPath as IndexPath)
                return cell
                
            }
        }else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "emptyCell", for: indexPath as IndexPath)
            return cell
        }
    }
    
    //MARK: Actions
    @IBAction func finishButton(_ sender: UIButton) {
        ProxyInterface.sharedInstance.postScoreFunc(username, score: String( Score.finalScore), completion: {(retVal) -> Void in
            OperationQueue.main.addOperation {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Finish_ID") as! FinalViewController
                self.present(vc, animated: true, completion: nil)
            }
        })
    }

    
}
