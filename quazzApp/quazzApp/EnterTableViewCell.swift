//
//  EnterTableViewCell.swift
//  quazzApp
//
//  Created by Milica Ignjatovic on 9/4/16.
//  Copyright © 2016 Milica Ignjatović. All rights reserved.
//

import UIKit

class EnterTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
   
    @IBOutlet weak var ans: UITextField!
    
    @IBOutlet weak var correctAnswer: UILabel!
    
    var  correctAns : String = " "
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.ans.addTarget(
            self,
            action: #selector(score),
            for: UIControlEvents.editingChanged
        )
        correctAnswer.isHidden = true
    }
    
    func score(){
        if ans.text!.lowercased().isEqual(correctAns.lowercased()){
            Score.finalScore = Score.finalScore + 1
            backgroundColor = UIColor.green
            correctAnswer.isHidden = true
        }else{
            correctAnswer.text = correctAns
            backgroundColor = UIColor.red
            ans.isUserInteractionEnabled = false
            correctAnswer.isHidden = false

        }
    }
    
    @IBAction func didFinishEditing(_ sender: UITextField) {
        self.score()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
