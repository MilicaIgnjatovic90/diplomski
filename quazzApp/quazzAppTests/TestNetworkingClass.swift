//
//  TestNetworkingClass.swift
//  quazzApp
//
//  Created by Milica Ignjatovic on 2/17/17.
//  Copyright © 2017 Milica Ignjatović. All rights reserved.
//

import XCTest
@testable import quazzApp

class TestNetworkingClass: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testLoginFuncTrue() {
        ProxyInterface.sharedInstance.loginFunc("Test", password: "test", completion:{(retVal) -> Void in
            XCTAssertEqual(retVal,"True")
        })
    }
    
    func testLoginFuncFalse() {
        ProxyInterface.sharedInstance.loginFunc("npr", password: "npr", completion:{(retVal) -> Void in
            XCTAssertEqual(retVal,"user not found")
        })
    }
    
    func testRegisterFuncTrue() {
        ProxyInterface.sharedInstance.registerFunc("test", password: "test", email: "test@gmail.com", completion:{(retVal) -> Void in
            XCTAssertEqual(retVal,"True")
        })
    }
    
    func testRegisterFuncFalse() {
        ProxyInterface.sharedInstance.registerFunc("Test", password: "test", email: "test@gmail.com", completion:{(retVal) -> Void in
            XCTAssertEqual(retVal,"Test")
        })
    }
    
    func testPostScoreTrue() {
        ProxyInterface.sharedInstance.postScoreFunc("Test", score: "8", completion:{(retVal) -> Void in
            XCTAssertEqual(retVal,"True")
        })
    }
    
    func testPostScoreFalse() {
        ProxyInterface.sharedInstance.postScoreFunc("Test", score: "8", completion:{(retVal) -> Void in
            XCTAssertEqual(retVal,"True")
        })
    }

    func testScoreFuncFalse() {
        ProxyInterface.sharedInstance.scoreFunc("npr", password: "npr", completion:{(retVal) -> Void in
            XCTAssertEqual(retVal,"False")
        })
    }
    
    func testScoreFuncTrueWrongValue() {
        ProxyInterface.sharedInstance.scoreFunc("npr", password: "npr", completion:{(retVal) -> Void in
            XCTAssertNotEqual(retVal,"10")
        })
    }

    func testScoreFuncTrue() {
        ProxyInterface.sharedInstance.scoreFunc("npr", password: "npr", completion:{(retVal) -> Void in
            XCTAssertEqual(retVal,"8")
        })
    }
    
    func testReturnQuestionsTrue() {
        let expectedNum = Int(UserDefaultsHelper().readUserDefaults())
      
        ProxyInterface.sharedInstance.getQuestions(completion:{(retVal) -> Void in
            let countQuestions = Questions().parsingQue(getQuestions: retVal).count
            XCTAssertEqual(countQuestions,expectedNum)
        })
    }
    
    func testReturnQuestionsFalse() {
        ProxyInterface.sharedInstance.getQuestions(completion:{(retVal) -> Void in
            let countQuestions = Questions().parsingQue(getQuestions: retVal).count
            XCTAssertNotEqual(countQuestions,3)
        })
    }
    
    func testReturnsData() {
        ProxyInterface.sharedInstance.getQuestions(completion:{(retVal) -> Void in
            XCTAssertNotNil(retVal)
        })
    }
    
    func testPerformanceScore() {
        self.measure {
            ProxyInterface.sharedInstance.scoreFunc("npr", password: "npr", completion:{(retVal) -> Void in
                
            })
        }
    }
    
    func testPerformanceGetQuestionsAll() {
        UserDefaultsHelper().writeUserDefaults(string: "79")
        self.measure {
            ProxyInterface.sharedInstance.getQuestions(completion:{(retVal) -> Void in
                let _ = Questions().parsingQue(getQuestions: retVal).count
               
            })
        }
    }
    
    func testPerformanceGetQuestionsOne() {
        UserDefaultsHelper().writeUserDefaults(string: "70")
        self.measure {
            ProxyInterface.sharedInstance.getQuestions(completion:{(retVal) -> Void in
                let _ = Questions().parsingQue(getQuestions: retVal).count
               
            })
        }
    }

}
