//
//  TableViewController.swift
//  quazzApp
//
//  Created by Milica Ignjatovic on 2/17/17.
//  Copyright © 2017 Milica Ignjatović. All rights reserved.
//

import XCTest
@testable import quazzApp

class TableViewController: XCTestCase {
    var storyboard = UIStoryboard(name: "Main", bundle:nil)
    var vc: UserTableViewController? = nil

    override func setUp() {
        super.setUp()
        storyboard = UIStoryboard(name: "Main", bundle:nil)
        vc = storyboard.instantiateViewController(withIdentifier: "UserTableViewController_ID") as? UserTableViewController
        _ = vc?.view

    }
    
    override func tearDown() {
       self.vc = nil
        super.tearDown()
    }
    
    // View loading tests
    func testThatViewLoads()
    {
        XCTAssertNotNil(self.vc?.view, "View not initiated properly");
    }
    
    
    func testThatTableViewLoads()
    {
        XCTAssertNotNil(self.vc?.tableView, "TableView not initiated");
    }
    
    //UITableView tests
    func testThatViewConformsToUITableViewDataSource()
    {
        XCTAssertTrue((self.vc?.conforms(to: UITableViewDataSource.self))!)
    }
    
    func testThatTableViewHasDataSource()
    {
        XCTAssertNotNil(self.vc?.tableView.dataSource, "Table datasource cannot be nil");
    }
    
    func testThatViewConformsToUITableViewDelegate()
    {
        XCTAssertTrue((self.vc?.conforms(to: UITableViewDelegate.self))!)
    }
    
   func testTableViewIsConnectedToDelegate()
    {
        XCTAssertNotNil(self.vc?.tableView.delegate, "Table delegate cannot be nil")
    }

}
