//
//  quazzAppUITests.swift
//  quazzAppUITests
//
//  Created by Milica Ignjatovic on 8/29/16.
//  Copyright © 2016 Milica Ignjatović. All rights reserved.
//

import XCTest

class quazzAppUITests: XCTestCase {
    let app = XCUIApplication()
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app.launch()
        sleep(1)
        
    }
    
    override func tearDown() {
        super.tearDown()
    }
   
    func loginOrRegistersFunc(){
        let registerButton = app.buttons["Register"]
        let loginButton = app.buttons["Login"]
        XCTAssertTrue(app.staticTexts["Login or Register"].exists)
        XCTAssertTrue(registerButton.exists)
        XCTAssertTrue(loginButton.exists)
    }
    
    func testLoginOrRegister() {
        self.loginOrRegistersFunc()
    }
    
    func registerFunc(){
        let registerButton = app.buttons["Register"]
        registerButton.tap()

        XCTAssertTrue(app.staticTexts["Please enter your data:"].exists)
        XCTAssertTrue(app.staticTexts["Username"].exists)
        XCTAssertTrue(app.staticTexts["Password"].exists)
        XCTAssertTrue(app.staticTexts["E - mail"].exists)
      
        XCTAssertTrue(app.textFields.element(boundBy: 0).exists)
        XCTAssertTrue(app.textFields.element(boundBy: 1).exists)
        XCTAssertTrue(app.textFields.element(boundBy: 2).exists)
    }
    
    func testRegister() {
        self.registerFunc()
    }
    
    func loginFunc(){
        let loginButton = app.buttons["Login"]
      
        loginButton.tap()
        loginButton.tap()

        XCTAssertTrue(app.staticTexts["Enter username and password:"].exists)
        XCTAssertTrue(app.staticTexts["Username"].exists)
        XCTAssertTrue(app.staticTexts["Password"].exists)
        XCTAssertTrue(app.buttons["Login As Admin"].exists)
        XCTAssertTrue(app.textFields.element(boundBy: 0).exists)
        XCTAssertTrue(app.textFields.element(boundBy: 0).exists)
        XCTAssertTrue(loginButton.exists)
    }
    
    
    func testLogin(){
        self.loginFunc()
    }
    
    func firstScreen(){
        XCTAssertNotNil(app.staticTexts.element(boundBy: 0).label)
        XCTAssertNotNil(app.staticTexts.element(boundBy: 1).label)
        XCTAssertNotNil(app.staticTexts.element(boundBy: 2).label)
        XCTAssertNotNil(app.staticTexts.element(boundBy: 3).label)
        
        let selectedFormationLabel = app.staticTexts["Add new question:"]
        XCTAssertTrue(selectedFormationLabel.exists)
        
        XCTAssertEqual("Add new question:", app.staticTexts.element(boundBy: 0).label)
        XCTAssertEqual("Question",app.staticTexts.element(boundBy: 1).label)
        XCTAssertEqual("Type of question", app.staticTexts.element(boundBy: 2).label)
        XCTAssertEqual("Offered answers", app.staticTexts.element(boundBy: 4).label)
        XCTAssertEqual("Correct answer", app.staticTexts.element(boundBy: 3).label)
        
        let addqueButton = app.buttons["addque"]
        
        XCTAssertTrue(addqueButton.textFields.element(boundBy: 0).exists)
        XCTAssertTrue(addqueButton.textFields.element(boundBy: 1).exists)
        XCTAssertTrue(addqueButton.textFields.element(boundBy: 2).exists)
        app.swipeLeft()
    }
    
    func secondScreen(){
        XCTAssertEqual("Please enter user data:", app.staticTexts.element(boundBy: 0).label)
        XCTAssertEqual("Username",app.staticTexts.element(boundBy: 1).label)
        XCTAssertEqual("Password", app.staticTexts.element(boundBy: 2).label)
        XCTAssertEqual("E - mail", app.staticTexts.element(boundBy: 3).label)
        XCTAssertEqual("Add user", app.buttons.element(boundBy: 0).label)

        XCTAssertTrue(app.textFields.element(boundBy: 0).exists)
        XCTAssertTrue(app.textFields.element(boundBy: 1).exists)
        XCTAssertTrue(app.textFields.element(boundBy: 2).exists)
        
        app.swipeLeft()
    }
    
    
    func thirdScreen(){
        XCTAssertEqual("Enter username and password:", app.staticTexts.element(boundBy: 0).label)
        XCTAssertEqual("Username",app.staticTexts.element(boundBy: 1).label)
        XCTAssertEqual("Password", app.staticTexts.element(boundBy: 2).label)
        
        XCTAssertTrue(app.textFields.element(boundBy: 0).exists)
        XCTAssertTrue(app.textFields.element(boundBy: 1).exists)
        
        XCTAssertTrue(app.staticTexts["0"].exists)
        XCTAssertTrue(app.buttons["Score"].exists)
        app.buttons["Score"].tap()
    }
    
    func forthScreen(){
        
    }
    
    func testLoginAsAdmin(){
        
        let login = app.buttons["Login"]
        login.tap()

        let loginAsAdmin = app.buttons["Login As Admin"]
       
        let textFieldUsername = app.textFields.element(boundBy: 0)
        textFieldUsername.tap()
        textFieldUsername.typeText("admin")
    
        let textFieldPassword = app.textFields.element(boundBy: 1)
        textFieldPassword.tap()
        textFieldPassword.typeText("admin")

        loginAsAdmin.tap()
        
        firstScreen()
        secondScreen()
        thirdScreen()
    }
    
    func testNavigationLeftAtLoginAsAdmin(){
        let login = app.buttons["Login"]
        login.tap()
        
        let loginAsAdmin = app.buttons["Login As Admin"]
        
        let textFieldUsername = app.textFields.element(boundBy: 0)
        textFieldUsername.tap()
        textFieldUsername.typeText("admin")
        
        let textFieldPassword = app.textFields.element(boundBy: 1)
        textFieldPassword.tap()
        textFieldPassword.typeText("admin")

        loginAsAdmin.tap()

        app.swipeLeft()
        app.swipeLeft()
        app.swipeLeft()
        firstScreen()
        
    }
 
    func testNavigationRightAtLoginAsAdmin(){
        
        let login = app.buttons["Login"]
        login.tap()
        
        let loginAsAdmin = app.buttons["Login As Admin"]
        
        let textFieldUsername = app.textFields.element(boundBy: 0)
        textFieldUsername.tap()
        textFieldUsername.typeText("admin")
        
        let textFieldPassword = app.textFields.element(boundBy: 1)
        textFieldPassword.tap()
        textFieldPassword.typeText("admin")
        
        loginAsAdmin.tap()
        
        app.swipeRight()
        app.swipeRight()
        app.swipeRight()
        firstScreen()
    }

    
    func testLogoutFromLoginAsAdmin(){
        
        let login = app.buttons["Login"]
        login.tap()
        
        let loginAsAdmin = app.buttons["Login As Admin"]
        
        let textFieldUsername = app.textFields.element(boundBy: 0)
        textFieldUsername.tap()
        textFieldUsername.typeText("admin")
        
        let textFieldPassword = app.textFields.element(boundBy: 1)
        textFieldPassword.tap()
        textFieldPassword.typeText("admin")
        
        loginAsAdmin.tap()
        
        app.swipeLeft()
        
        self.secondScreen()
        app.buttons["Logout"].tap()
        self.loginOrRegistersFunc()
        
    }
    
    func testUserScreen(){
        
        let login = app.buttons["Login"]
        login.tap()
        
        let loginButton = app.buttons["Login"]
        
        let textFieldUsername = app.textFields.element(boundBy: 0)
        textFieldUsername.tap()
        textFieldUsername.typeText("test")
        
        let textFieldPassword = app.textFields.element(boundBy: 1)
        textFieldPassword.tap()
        textFieldPassword.typeText("test")
        
        loginButton.tap()
        
//        let tablesQuery = app.tables
        
        app.buttons["Finish"].tap()
        
        XCTAssertTrue(app.staticTexts["Your score is: "].exists)
        
        app.buttons["Logout"].tap()
        
        self.testLoginOrRegister()
    }
}
