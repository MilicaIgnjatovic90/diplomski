from QuizServerClasses import QuizRequestHandler
import DatabaseHandler
import json
from urlparse import urlparse

class QuizGetQuestions(QuizRequestHandler):
    def getResponseToRequest(self, quizRequest):
        print ("call:")
        body_unicode = quizRequest.body.decode('utf-8')
        body = json.loads(body_unicode)
        numOfRecords = int(body['questionNum'])
        q = DatabaseHandler.returnQuestions(self, numOfRecords)
        return {"val": q}